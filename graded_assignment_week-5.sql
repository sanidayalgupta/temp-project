create database travel;

use travel;

create table PASSENGER(Passenger_name varchar(20), 
                       Category varchar(20),
                       Gender varchar(20),
					   Boarding_City varchar(20),
                       Destination_City varchar(20),
                       Distance int,
                       Bus_Type varchar(20)
);

create table PRICE(Bus_Type varchar(20),
				   Distance int,
				   Price int
);

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

-- 1. How many female and how many male passengers travelled for a minimum distance of 600 KMs?
 select Gender, COUNT(*) FROM passenger where Distance>=600 GROUP BY Gender;

-- 2. Find the minimum ticket price for Sleeper Bus. 
select * from price where price=(select min(price) from price where Bus_Type='sleeper');

-- 3. Select passenger names whose names start with character 'S'.
select *from passenger where  passenger_name like 'S%';

-- 4. Calculate price charged for each passenger displaying Passenger name, Boarding City, 
-- Destination City, Bus_Type, Price in the output.

select p.*,pr.price from passenger p inner join price pr on p.Distance=pr.Distance and p.Bus_Type=pr.Bus_Type;
          

-- 5. What is the passenger name and his/her ticket price who travelled in Sitting bus for a 
-- distance of 1000 KMs. 

select p.Passenger_name,pr.price from passenger p inner join price pr on p.Distance=pr.Distance and p.Bus_Type=pr.Bus_Type and p.Distance=1000;


-- 6. What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji?

select p.Passenger_name,p.Boarding_City,p.Destination_City,pr.Bus_Type,pr.price from passenger p inner join price pr on p.distance=pr.Distance where p.Passenger_name='pallavi';

-- 7. List the distances from the "Passenger" table which are unique (non-repeated distances) 
-- in descending order.
select distinct Distance from passenger order by Distance desc;

-- 8. Display the passenger name and percentage of distance travelled by that passenger from the  
-- total distance travelled by all passengers without using user variables. 

select Passenger_name,Distance * 100 / t.s AS '% of total Travelled Distance' FROM passenger CROSS JOIN (SELECT SUM(Distance) AS s FROM passenger) t;


-- 9. Create a view to see all passengers who travelled in AC Bus.

create view AC_Travelers as select * from passenger where category='AC';
select * from AC_Travelers;

-- 10. Create a stored procedure to find total passengers traveled using Sleeper buses.

DELIMITER //
CREATE  PROCEDURE Passenger_list ()
BEGIN
select Passenger_name,Bus_Type
from passenger
where Bus_Type="Sleeper";
END //
DELIMITER ;
Call Passenger_list;

-- 11. Display 5 records at one time
select * from passenger limit 5;


